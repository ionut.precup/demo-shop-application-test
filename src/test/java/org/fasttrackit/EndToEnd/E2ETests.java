package org.fasttrackit.EndToEnd;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.Account;
import org.fasttrackit.body.Modal;
import org.fasttrackit.checkout.CheckoutCompletePage;
import org.fasttrackit.checkout.CheckoutInfoPage;
import org.fasttrackit.checkout.CheckoutSummaryPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class E2ETests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }
    @Test (testName = "User is able to complete order as guest",
            description = "Testing that unlogged user is able to complete order")
    @Severity(SeverityLevel.NORMAL)
    public void verify_that_user_can_complete_order_as_guest_test () {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p1);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();
        checkoutSummaryPage.clickOnCompleteYourOrderButton();
        CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();
        assertEquals(checkoutCompletePage.getCheckoutCompletePageTitle(), "Order complete", "Expected Checkout Complete Page Title to be: Order complete");
        assertEquals(checkoutCompletePage.getCheckoutCompletePageMessage(), "Thank you for your order!", "Expected Checkout Complete Page Title to be: Thank you for your order!");
    }

    @Test (testName = "Logged in user is able to complete order",description = "Testing that logged in user is able to complete order",
            dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class)
    @Severity(SeverityLevel.BLOCKER)
    public void verify_that_logged_in_user_can_complete_order_test (Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        Product p2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p2);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();
        checkoutSummaryPage.clickOnCompleteYourOrderButton();
        CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();
        assertEquals(checkoutCompletePage.getCheckoutCompletePageTitle(), "Order complete", "Expected Checkout Complete Page Title to be: Order complete");
        assertEquals(checkoutCompletePage.getCheckoutCompletePageMessage(), "Thank you for your order!", "Expected Checkout Complete Page Title to be: Thank you for your order!");
    }
    @Test (testName = "user is able to continue shopping after order is complete as guest",description = "Testing that unlogged user is able to continue shopping after order is complete")
    @Severity(SeverityLevel.NORMAL)
    public void verify_that_user_can_continue_shopping_after_complete_order_as_guest_test () {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p1);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();
        checkoutSummaryPage.clickOnCompleteYourOrderButton();
        CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();
        assertEquals(checkoutCompletePage.getCheckoutCompletePageMessage(), "Thank you for your order!", "Expected Checkout Complete Page Title to be: Thank you for your order!");
        checkoutCompletePage.continueShopping();
        assertEquals(homePage.getHeader().getHomePageTitle(),"Products", "Expected user to return to homepage and page title to be: Products");
        assertTrue(homePage.validateProductsListIsDisplayed(),"Expected products list to be displayed");

    }
    @Test (testName = "Logged in user is able to continue shopping after order is complete",description = "Testing that logged in user is able to continue shopping after order is complete",
            dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_that_logged_in_user_can__continue_shopping_after_complete_order_test (Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        Product p2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p2);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();
        checkoutSummaryPage.clickOnCompleteYourOrderButton();
        CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();
        assertEquals(checkoutCompletePage.getCheckoutCompletePageTitle(), "Order complete", "Expected Checkout Complete Page Title to be: Order complete");
        assertEquals(checkoutCompletePage.getCheckoutCompletePageMessage(), "Thank you for your order!", "Expected Checkout Complete Page Title to be: Thank you for your order!");
        checkoutCompletePage.continueShopping();
        assertEquals(homePage.getHeader().getHomePageTitle(),"Products", "Expected user to return to homepage and page title to be: Products");
        assertTrue(homePage.validateProductsListIsDisplayed(),"Expected products list to be displayed");
    }



}
