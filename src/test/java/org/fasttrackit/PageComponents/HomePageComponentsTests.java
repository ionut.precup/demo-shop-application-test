package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class HomePageComponentsTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Demo Shop Application Title test", description = "Testing that Demo Shop Application Title is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_demo_shop_app_title() {

        assertEquals(homePage.verifyThatMainPageTitleIsDisplayed(), "Demo shop", "Application title is expected to be Demo Shop.");
    }

    @Test (testName = "Header Logo icon test", description = "Testing that the Logo icon is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_header_contains_logo_icon() {
        assertTrue(homePage.getHeader().validateLogoIconIsDisplayed(), "Expected Logo icon to be displayed.");
    }
    @Test (testName = "Header Shopping Cart Icon test", description = "Testing that the Shopping Cart icon is displayed")
    @Severity(SeverityLevel.BLOCKER)
    public void verify_header_contains_shopping_cart_icon(){
        assertTrue(homePage.getHeader().validateShoppingCartIconIsDisplayed(), "Expected Shopping Cart icon to be displayed.");
    }
    @Test (testName = "Header Wishlist icon test", description = "Testing that the Wishlist icon is displayed")
    @Severity(SeverityLevel.NORMAL)
    public void verify_header_contains_wishlist_icon(){
        assertTrue(homePage.getHeader().validateWishlistIconIsDisplayed(), "Expected Wishlist icon to be displayed.");
    }
    @Test (testName = "Header Sign in icon test", description = "Testing that the Sign In icon is displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_header_contains_sign_in_icon(){
        assertTrue(homePage.getHeader().validateSignInIconIsDisplayed(), "Expected Sign In icon to be displayed.");
    }
    @Test (testName = "Greetings message test", description = "Testing that the Greetings message is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_header_greetings_message(){
        assertEquals(homePage.getHeader().validateGreetingsMessageIsDisplayed(), "Hello guest!", "Expected greetings message to be: Hello guest!");
    }

    @Test (testName = "Footer details info test", description = "Testing that the Footer details information are displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_footer_details_info() {
        assertEquals(homePage.getFooter().getDetails(), "Demo Shop | build date 2021-05-21 14:04:30 GTBDT", "Expected footer details to be: Demo Shop | build date 2021-05-21 14:04:30 GTBDT");
    }
    @Test (testName = "Footer Question icon test", description = "Testing that the Footer Question icon is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_footer_contains_question_icon(){
        assertTrue(homePage.getFooter().validateQuestionIconIsDisplayed(), "Expected Question Icon to be displayed");
    }
    @Test (testName = "Footer Reset icon test", description = "Testing that the Footer Reset icon is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_footer_contains_reset_icon(){
        assertTrue(homePage.getFooter().validateResetIconIsDisplayed(), "Expected Reset Icon to be displayed");
    }


    @Test (testName = "Main Page Search button test", description = "Testing that the Search button is displayed and enabled")
    @Severity(SeverityLevel.NORMAL)
    public void verify_search_button_is_displayed() {
        assertTrue(homePage.validateThatSearchButtonIsDisplayed(), "Expected search button to be displayed and enabled");
    }

    @Test (testName = "Main Page Sort field test", description = "Testing that the Sort field is displayed and enabled")
    @Severity(SeverityLevel.MINOR)
    public void verify_sort_field_is_displayed(){
        assertTrue(homePage.validateSortFieldIsDisplayed(),"Expected Sort Field to be displayed");
    }


}
