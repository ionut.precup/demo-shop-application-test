package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.CartPage;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class EmptyCartPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    CartPage cartPage = new CartPage();

    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Cart Page title test", description = "Testing that Cart Page Title is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_cart_page_title_is_displayed() {
        homePage.clickOnTheShoppingCartIcon();
        assertEquals(cartPage.getCartPageTitle(), "Your cart", "Expected cart page title to be: Your cart");
    }

    @Test(testName = "Empty Cart Message test", description = "Testing that Empty Cart Message is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_empty_cart_message_is_displayed() {
        homePage.clickOnTheShoppingCartIcon();
        assertEquals(cartPage.getCartPageMessage(), "How about adding some products in your cart?");
    }

}
