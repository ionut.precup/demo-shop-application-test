package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.checkout.CheckoutCompletePage;
import org.fasttrackit.checkout.CheckoutInfoPage;
import org.fasttrackit.checkout.CheckoutSummaryPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckoutCompletePageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
    CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();

    CheckoutCompletePage checkoutCompletePage = new CheckoutCompletePage();

    @BeforeMethod
    public void setup() {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "15.99 EUR"));
        p1.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p1);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        checkoutSummaryPage.clickOnCompleteYourOrderButton();
    }
    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }
    @Test(testName = "Checkout Complete Page Title test", description = "Testing that Checkout Complete Page Title is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_checkout_complete_page_title_is_displayed_test() {
        assertEquals(checkoutCompletePage.getCheckoutCompletePageTitle(), "Order complete", "Expected Checkout Complete Page Title to be: Order complete");
    }
    @Test(testName = "Checkout Complete Page Message test", description = "Testing that Checkout Complete Page message is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_checkout_complete_page_message_test() {
        assertEquals(checkoutCompletePage.getCheckoutCompletePageMessage(), "Thank you for your order!", "Expected Checkout Complete Page Title to be: Thank you for your order!");
    }
    @Test(testName = "Checkout Complete Page Continue Button test", description = "Testing that Continue Shopping Button is displayed")
    @Severity(SeverityLevel.NORMAL)
    public void verify_checkout_complete_page_contain_continue_shopping_button_test() {
       assertTrue(checkoutCompletePage.validateCheckoutCompleteContinueShoppingButtonIsDisplayed(),"Expected Continue Shopping Button to be displayed");
    }

}
