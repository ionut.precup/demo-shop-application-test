package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.checkout.CheckoutInfoPage;
import org.fasttrackit.checkout.CheckoutSummaryPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CheckoutSummaryPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();
    CheckoutSummaryPage checkoutSummaryPage = new CheckoutSummaryPage();

    @BeforeMethod
    public void setup() {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToBasket();
        ProductsInCartPage cartPage = new ProductsInCartPage(p1);
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Checkout Summary Page title test", description = "Testing that Checkout Summary Page Title is displayed ")
    @Severity(SeverityLevel.MINOR)
    public void verify_checkout_summary_page_title_test() {
        assertEquals(checkoutSummaryPage.getCheckoutSummaryPageTitle(), "Order summary", "Expected Checkout Summary Page Title to be: Order Summary");
    }

    @Test(testName = "Checkout Payment Information test", description = "Testing that Payment Information is displayed ")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_checkout_summary_page_payment_information_test() {
        assertEquals(checkoutSummaryPage.getCheckoutPaymentInformation(), "Cash on delivery", "Expected Payment Information to be:Cash on delivery");
    }

    @Test(testName = "Checkout Shipping Information test", description = "Testing that Shipping Information is displayed ")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_checkout_summary_page_shipping_information_test() {
        assertEquals(checkoutSummaryPage.getCheckoutShippingInformation(), "CHOO CHOO DELIVERY!", "Expected Shipping Information to be: CHOO CHOO DELIVERY!");
    }

    @Test(testName = "Checkout Summary Page Cancel Button test", description = "Testing that Cancel Button is displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_checkout_summary_page_contains_cancel_button_test() {
        assertTrue(checkoutSummaryPage.validateCheckoutCancelButtonIsDisplayed(), "Expected Cancel Button to be displayed");
    }

    @Test(testName = "Checkout Summary Page Complete your order Button test", description = "Testing that Complete your order Button exists and is displayed")
    @Severity(SeverityLevel.BLOCKER)
    public void verify_checkout_summary_page_contains_continue_checkout_button_test() {
        assertTrue(checkoutSummaryPage.validateCompleteYourOrderButtonIsDisplayed(), "Expected Complete your order Button to be displayed");

    }


}
