package org.fasttrackit.PageComponents;

import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.body.SortDropDownMenu;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SortMenuTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test (testName = "Sort menu elements are displayed test", description = "Testing that Sort Drop Down Menu elements are displayed")
    public void verify_sort_menu_elements() {
        assertTrue(homePage.validateSortFieldIsDisplayed(), "Expected Sort Field to be displayed");
        SortDropDownMenu sortDropDownMenu = new SortDropDownMenu();
        assertTrue(sortDropDownMenu.validateSortAtoZIsDisplayed(), "Expected Sort by name (A to Z) to be displayed by default");
        homePage.clickOnTheSortMenu();
        assertTrue(sortDropDownMenu.validateSortZtoAIsDisplayed(), "Expected Sort by name (Z to A) to be displayed");
        assertTrue(sortDropDownMenu.validateSortByPriceLowToHighIsDisplayed(),"Expected Sort by price (Low to High) to be displayed");
        assertTrue(sortDropDownMenu.validateSortByPriceHighToLowIsDisplayed(),"Expected Sort by price (High to Low) to be displayed");
        homePage.clickOnTheSortMenu();
    }

}
