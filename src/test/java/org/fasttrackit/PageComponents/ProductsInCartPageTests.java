package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProductsInCartPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }
    @Test(testName = "Products added to Cart are displayed",description = "Testing that shopping cart contains added products")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_products_in_cart_are_displayed () {
        Product p1 = new Product("1",new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToBasket();
        Product p2 = new Product("2",  new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToBasket();
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        p3.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(p3);
        assertTrue(cartPage.validateProductsInCartAreDisplayed(),"Expected all products added to cart to be displayed");

    }

    @Test(testName = "Cart Page Increment/Decrement Quantity buttons test",description = "Testing that Increment and Decrement buttons are displayed",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_increment_decrement_buttons_are_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.validateIncrementQuantityButtonIsDisplayed(), "Expected Increment Quantity Button to be displayed");
        assertTrue(cartPage.validateDecrementQuantityButtonIsDisplayed(), "Expected Decrement Quantity Button to be displayed");

    }

    @Test(testName = "Cart Page Product Quantity test",description = "Testing that Quantity value is displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_product_quantity_is_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertEquals(cartPage.getQuantity(), "1", "Expected Quantity for product in cart to be 1");


    }

    @Test(testName = "Cart Page Trash button test",description = "Testing that Trash button is displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_trash_button_is_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.validateTrashButtonIsDisplayed(), "Expected Trash Button to be displayed");
    }

    @Test(testName = "Cart Page Product Title test",description = "Testing that Product title is displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_product_title_is_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.validateProductCartTitleIsDisplayed(), "Expected Product Title to be displayed");
    }

    @Test(testName = "Cart Page Continue Shopping Button test",description = "Testing that Continue Shopping button is displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_continue_shopping_button_is_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.validateContinueShoppingButtonIsDisplayed(), "Expected Continue Shopping Button to be displayed");
    }

    @Test(testName = "Cart Page Checkout Button test", description = "Testing that Checkout button is displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.BLOCKER)
    public void verify_checkout_button_is_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertTrue(cartPage.validateCheckoutButtonIsDisplayed(), "Expected Checkout Button to be displayed");
    }

    @Test(testName = "Cart Page Product Unit Price test", description = "Testing that Product Price is correctly displayed ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_product_unit_price_is_correctly_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        String expectedPrice = product.getExpectedResults().getPrice();
        assertEquals(cartPage.getUnitPrice(), expectedPrice, "Expected Product Cart Price to be:" + expectedPrice);

    }




}
