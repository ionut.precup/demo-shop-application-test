package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductDetailsPage;
import org.fasttrackit.products.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.elements;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class ProductDetailPageTests extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Product Detail Page components - Product Expected Title test", dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class,
            description = "Testing that user is able to click on a product and product title is correctly displayed" )
    @Severity(SeverityLevel.NORMAL)
    public void product_title_is_correctly_displayed_test(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedTitle = p.getExpectedResults().getTitle();
        assertEquals(productPage.getTitle(), expectedTitle, "Expected title to be " + expectedTitle);

    }
    @Test(testName = "Product Detail Page components - Product Expected Price test", dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class,
            description = "Testing that user is able to click on a product and product price is correctly displayed" )
    @Severity(SeverityLevel.NORMAL)
    public void product_price_is_correctly_displayed_test(Product p) {
        p.clickOnProduct();
        ProductDetailsPage productPage = new ProductDetailsPage();
        String expectedPrice = p.getExpectedResults().getPrice();
        assertEquals(productPage.getPrice(), expectedPrice, "Expected price to be " + expectedPrice);

    }


}
