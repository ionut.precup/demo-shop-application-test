package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.WishlistPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WishlistPageTests {
    MainPage homePage = new MainPage();
    WishlistPage wishlistPage = new WishlistPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }

    @Test (testName = "Wishlist Page Title test", description = "Testing that WishList Page Title is displayed")
    @Severity(SeverityLevel.MINOR)
    public void verify_wishlist_page_tittle_is_displayed() {
        homePage.clickOnTheWishlistButton();
        assertEquals(wishlistPage.getPageSubtitle(), "Wishlist", "Expected Wishlist page title to be: Wishlist");

    }
    @Test(testName = "Wishlist Page test", description = "Testing that WishList Page contains added products")
    @Severity(SeverityLevel.NORMAL)
    public void verify_wishlist_page_contain_products() {
        Product p1 = new Product("1",new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToWishlist();
        Product p2 = new Product("2",  new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToWishlist();
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        p3.addProductToWishlist();
        homePage.clickOnTheWishlistButton();
        assertTrue(wishlistPage.validateProductsInWishlistAreDisplayed(),"Expected wishlist to contain products");

    }


}
