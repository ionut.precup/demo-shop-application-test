package org.fasttrackit.PageComponents;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class ModalComponentsTests extends TestConfiguration {

    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
        homePage.returnToHomePage();
    }
    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }
    @Test(testName = "Modal components test", description = "Testing that modal components are displayed and modal can be closed")
    @Severity(SeverityLevel.NORMAL)
    public void modal_components_are_displayed_and_modal_can_be_closed_test() {
        homePage.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal is displayed.");
        Modal modal = new Modal();
        assertEquals(modal.getModalTitle(), "Login", "Expected Modal title to be Login.");
        assertTrue(modal.validateCloseButtonIsDisplayed(), "Expected Close Button to be displayed.");
        assertTrue(modal.validateUsernameFieldIsDisplayed(), "Expected Username field to be displayed.");
        assertTrue(modal.validatePasswordFieldIsDisplayed(), "Expected Password field to be displayed.");
        assertTrue(modal.validateLoginButtonIsDisplayedAndIsEnabled(), "Expected Login Button to be displayed and enabled");
        modal.clickOnCloseButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");

    }


}
