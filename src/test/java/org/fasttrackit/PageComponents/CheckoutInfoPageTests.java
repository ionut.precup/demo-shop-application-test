package org.fasttrackit.PageComponents;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.checkout.CheckoutInfoPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CheckoutInfoPageTests extends TestConfiguration {
    Product p2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
    MainPage homePage = new MainPage();
    ProductsInCartPage cartPage = new ProductsInCartPage(p2);
    CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();


    @BeforeMethod
    public void setup() {
        p2.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }


    @Test(testName = "Checkout Info Page title test", description = "Testing that Checkout Info Page Title is displayed ")
    @Severity(SeverityLevel.MINOR)
    public void verify_checkout_info_page_title_test() {
        assertEquals(checkoutInfoPage.getCheckoutInfoPageTitle(), "Your information", "Expected Checkout Info Page title to be:Your Information");
    }

    @Test(testName = "Checkout Info Page subtitles test", description = "Testing that Checkout Info Page Subtitles are displayed ")
    @Severity(SeverityLevel.MINOR)
    public void verify_checkout_info_page_subtitles_test() {
        assertEquals(checkoutInfoPage.getAddressInformation(), "Address information");
        assertEquals(checkoutInfoPage.getDeliveryInformation(), "Delivery information");
        assertEquals(checkoutInfoPage.getPaymentInformation(), "Payment information");
    }

    @Test(testName = "Input fields tests", description = "Testing that all Input Fields are displayed and enabled")
    @Severity(SeverityLevel.NORMAL)
    public void verify_input_fields_test() {
        assertTrue(checkoutInfoPage.validateFirstNameInputFieldIsEnabled(), "Expected First Name Input Field to be displayed and enabled");
        assertTrue(checkoutInfoPage.validateLastNameInputFieldIsEnabled(), "Expected Last Name Input Field to be displayed and enabled");
        assertTrue(checkoutInfoPage.validateAddressInputFieldIsEnabled(), "Expected Address Input Field to be displayed and enabled");
    }

    @Test(testName = "Delivery Type option test", description = "Testing that Choo Choo Delivery Type option is displayed and selected by default")
    @Severity(SeverityLevel.NORMAL)
    public void verify_delivery_type_option_test() {
        assertTrue(checkoutInfoPage.validateDeliveryTypeOptionIsSelected(), "Expected Choo Choo Delivery Type option to be displayed and selected");
    }

    @Test(testName = "Payment Type options test - Cash on delivery", description = "Testing that Cash on delivery Payment option is displayed and selected by default")
    @Severity(SeverityLevel.NORMAL)
    public void verify_cash_on_delivery_payment_type_option_test() {
        assertTrue(checkoutInfoPage.validateCashOnDeliveryOptionIsSelected(), "Expected Cash on Delivery payment option to be displayed and selected by default");
    }

    @Test(testName = "Payment Type options test - Credit Card", description = "Testing that Credit card payment option is enabled ")
    @Severity(SeverityLevel.NORMAL)
    public void verify_credit_card_payment_type_test() {
        assertTrue(checkoutInfoPage.validateCreditCardOptionIsEnabled(), "Expected Credit card Payment option to be displayed ");
    }

    @Test(testName = "Payment Type options test - Paypal", description = "Testing that Paypal payment option is enabled ")
    @Severity(SeverityLevel.NORMAL)
    public void verify_paypal_payment_type_test() {
        assertTrue(checkoutInfoPage.validatePayPalOptionIsEnabled(), "Expected Paypal Payment option to be displayed ");
    }


    @Test(testName = "Checkout Info Page Cancel Button test", description = "Testing that Cancel Button exists and is displayed")
    @Severity(SeverityLevel.NORMAL)
    public void verify_checkout_info_page_contains_cancel_button_test() {
        assertTrue(checkoutInfoPage.validateCancelButtonIsDisplayed(), "Expected Cancel Button to be displayed");

    }

    @Test(testName = "Checkout Info Page Continue checkout Button test", description = "Testing that Continue checkout Button exists and is displayed")
    @Severity(SeverityLevel.CRITICAL)
    public void verify_checkout_info_page_contains_continue_checkout_button_test() {
        assertTrue(checkoutInfoPage.validateContinueCheckoutButtonIsDisplayed(), "Expected Continue checkout Button to be displayed");
    }


}
