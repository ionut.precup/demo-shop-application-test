package org.fasttrackit.dataprovider;

import org.fasttrackit.Account;
import org.testng.annotations.DataProvider;

public class AccountDataProvider {

    @DataProvider(name = "AccountDataProvider")
    public Object[][] createAccountsProvider() {
        Account beetleAccount = new Account("beetle", "choochoo");
        Account dinoAccount = new Account("dino", "choochoo");
        Account turtleAccount = new Account("turtle", "choochoo");
        return new Object[][]{
                {beetleAccount},
                {dinoAccount},
                {turtleAccount}
        };
    }
}
