package org.fasttrackit.dataprovider;

import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.DataProvider;

public class ProductDataProvider {
    @DataProvider(name = "productsDataProvider")
    public static Object[][] createProductsProvider() {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        Product p2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        Product p4 = new Product("4", new ProductExpectedResults("Practical Wooden Bacon", "$29.99"));
        Product p5 = new Product("5", new ProductExpectedResults("Awesome Soft Shirt", "$29.99"));
        Product p6 = new Product("6", new ProductExpectedResults("Practical Wooden Bacon", "$1.99"));
        Product p7 = new Product("7", new ProductExpectedResults("Practical Metal Mouse", "$9.99"));
        Product p8 = new Product("8", new ProductExpectedResults("Licensed Steel Gloves", "$14.99"));
        Product p9 = new Product("9", new ProductExpectedResults("Gorgeous Soft Pizza", "$19.99"));
        Product p10 = new Product("0", new ProductExpectedResults("Refined Frozen Mouse", "$9.99"));
        return new Object[][]{
                {p1},
                {p2},
                {p3},
                {p4},
                {p5},
                {p6},
                {p7},
                {p8},
                {p9},
                {p10},
        };
    }

}
