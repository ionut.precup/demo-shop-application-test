package org.fasttrackit.Scenarios;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.SortDropDownMenu;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.SearchPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "User can search for products by Product Title test",
            description = "Testing Search functionality based on Product Title ",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void verify_search_functionality_by_product_title(Product product) {
        homePage.clickOnTheSearchField();
        homePage.typeInSearchField(product.getExpectedResults().getTitle());
        homePage.clickOnTheSearchButton();
        List<String> afterSearchProductTitleList = new ArrayList<>();
        for (WebElement p: homePage.getProductTitleList()) {
            afterSearchProductTitleList.add(String.valueOf(p.getText()));
        }
        assertTrue(afterSearchProductTitleList.contains(product.getExpectedResults().getTitle()),"Expected Search results by product title to be relevant");

    }
    @Test(testName = "User can search for products by keyword test",
            description = "Testing Search functionality based on keyword")
    @Severity(SeverityLevel.NORMAL)
    public void verify_search_functionality_by_keyword() {
        homePage.clickOnTheSearchField();
        homePage.typeInSearchField("Awesome");
        homePage.clickOnTheSearchButton();
        List<String> afterSearchProductTitleList = new ArrayList<>();
        for (WebElement p: homePage.getProductTitleList()) {
            afterSearchProductTitleList.add(String.valueOf(p.getText()));
        }
        boolean expectedSearchResult = afterSearchProductTitleList.stream().toList().toString().contains("Awesome");
        assertTrue(expectedSearchResult);
        System.out.println(afterSearchProductTitleList.stream().toList());

    }
    @Test(testName = "User can search for products by first letters",
            description = "Testing Search functionality based on first letters")
    @Severity(SeverityLevel.NORMAL)
    public void verify_search_functionality_by_first_letters() {
        homePage.clickOnTheSearchField();
        homePage.typeInSearchField("met");
        homePage.clickOnTheSearchButton();
        List<String> afterSearchProductTitleList = new ArrayList<>();
        for (WebElement p: homePage.getProductTitleList()) {
            afterSearchProductTitleList.add(String.valueOf(p.getText()));
        }
        boolean expectedSearchResult = afterSearchProductTitleList.stream().toList().toString().contains("Met");
        assertTrue(expectedSearchResult);
        System.out.println(afterSearchProductTitleList.stream().toList());


    }


}

