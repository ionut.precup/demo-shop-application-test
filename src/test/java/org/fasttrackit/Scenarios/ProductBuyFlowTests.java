package org.fasttrackit.Scenarios;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProductBuyFlowTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Add product to cart test",description = "Testing that Shopping Badge Icon is displayed and count of products is correct when adding products one by one",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_add_product_to_cart(Product p) {
        p.addProductToBasket();
        assertTrue(homePage.getHeader().validateShoppingBadgeIconIsDisplayed(), "Expected Shopping Badge icon to be displayed");
        assertEquals(homePage.getHeader().getShoppingBadgeIcon(), "1", "Expected Shopping Badge number to be: 1");
    }

    @Test(testName = "Add product to wishlist test",description = "Testing that Wishlist Badge Icon is displayed and count of products is correct when adding products one by one",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_product_to_wishlist(Product p) {
        p.addProductToWishlist();
        assertTrue(homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to be displayed");
        assertEquals(homePage.getHeader().getWishlistBadgeIcon(), "1","Expected Wishlist Badge number to be: 1");
    }
    @Test(testName = "Add multiple products to cart test",description = "Testing that Shopping Badge Icon is displayed and count of products is correct when adding several products")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_multiple_products_to_cart() {
        Product p1 = new Product("1",new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToBasket();
        Product p2 = new Product("2",  new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToBasket();
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        p3.addProductToBasket();
        assertTrue(homePage.getHeader().validateShoppingBadgeIconIsDisplayed(), "Expected Shopping Badge icon to be displayed");
        assertEquals(homePage.getHeader().getShoppingBadgeIcon(), "3", "Expected Shopping Badge number to be:3");


    }
    @Test(testName = "Add  multiple products to wishlist test",description = "Testing that Wishlist Badge Icon is displayed and count of products is correct when adding several products")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_multiple_products_to_wishlist() {
        Product p1 = new Product("1",new ProductExpectedResults("Awesome Granite Chips", "$15.99"));
        p1.addProductToWishlist();
        Product p2 = new Product("2",  new ProductExpectedResults("Incredible Concrete Hat", "$7.99"));
        p2.addProductToWishlist();
        Product p3 = new Product("3", new ProductExpectedResults("Awesome Metal Chair", "$15.99"));
        p3.addProductToWishlist();
        assertTrue(homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to be displayed");
        assertEquals(homePage.getHeader().getWishlistBadgeIcon(), "3");

    }
    @Test(testName = "Add the same product to basket several times test",description = "Testing that Shopping Badge Icon is displayed and count of products is correct when adding same product several times")
    @Severity(SeverityLevel.NORMAL)
    public void user_can_add_same_product_to_basket_several_times() {
        Product p1 = new Product("1", new ProductExpectedResults("Awesome Granite Chips", "15.99 EUR"));
        p1.addProductToBasket();
        p1.addProductToBasket();
        p1.addProductToBasket();
        p1.addProductToBasket();
        p1.addProductToBasket();
        assertTrue(homePage.getHeader().validateShoppingBadgeIconIsDisplayed(), "Expected Shopping Badge icon to be displayed");
        assertEquals(homePage.getHeader().getShoppingBadgeIcon(), "5", "Expected Shopping Badge number to be:5");
    }
    @Test(testName = "Remove product from wishlist test", description ="Testing that user is able to remove product from Wishlist",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.NORMAL)
    public void user_can_remove_product_from_wishlist(Product p) {
        p.addProductToWishlist();
        p.removeProductFromWishlist();
        assertTrue(!homePage.getHeader().validateWishlistBadgeIconIsDisplayed(), "Expected wishlist badge icon to disappear");

    }


}
