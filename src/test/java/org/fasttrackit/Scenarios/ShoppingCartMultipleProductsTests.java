package org.fasttrackit.Scenarios;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.Account;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ShoppingCartMultipleProductsTests extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
    }
    @AfterClass
    public void clean () {
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "Cart Page Items Total test",description = "Testing that Items Total is correctly calculated with more than one product added to cart",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void verify_items_total_is_correctly_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        List<Double> cartPriceList = new ArrayList<>();
        for (WebElement p : cartPage.getCartPriceList()) {
            cartPriceList.add(Double.valueOf(p.getText().replace("$","")));
        }
        Stream<Double> doubleStream = cartPriceList.stream();
        DoubleSummaryStatistics doubleSummaryStatistics = doubleStream.collect(Collectors.summarizingDouble(e->e));
        System.out.println(doubleSummaryStatistics.getSum());
        assertEquals(cartPage.actualItemsTotal(),doubleSummaryStatistics.getSum(),"Expected Items Total to be correctly calculated");
    }


}
