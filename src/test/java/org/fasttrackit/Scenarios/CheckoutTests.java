package org.fasttrackit.Scenarios;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.checkout.CheckoutInfoPage;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.fasttrackit.products.ProductExpectedResults;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CheckoutTests extends TestConfiguration {
    Product p2 = new Product("2", new ProductExpectedResults("Incredible Concrete Hat", "7.99 EUR"));
    MainPage homePage = new MainPage();
    ProductsInCartPage cartPage = new ProductsInCartPage(p2);
    CheckoutInfoPage checkoutInfoPage = new CheckoutInfoPage();


    @BeforeMethod
    public void setup() {
        p2.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        cartPage.goToCheckout();
    }

    @AfterMethod
    public void returnToHomePage() {
        homePage.returnToHomePage();
        homePage.clickOnTheResetIcon();
    }

    @Test(testName = "First Name field is mandatory", description = "Testing that error message is displayed when the user leaves First Name field blank")
    @Severity(SeverityLevel.NORMAL)
    public void user_cant_continue_checkout_without_first_name_test() {
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMessage(), "First Name is required", "Expected Error Message to be:First Name is required");
    }

    @Test(testName = "Last Name field is mandatory", description = "Testing that error message is displayed when the user leaves Last Name field blank")
    @Severity(SeverityLevel.NORMAL)
    public void user_cant_continue_checkout_without_last_name_test() {
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInAddressField("Bistrita");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMessage(), "Last Name is required", "Expected Error Message to be:Last Name is required");
    }

    @Test(testName = "Address field is mandatory", description = "Testing that error message is displayed when the user leaves Address field blank")
    @Severity(SeverityLevel.NORMAL)
    public void user_cant_continue_checkout_without_address_test() {
        checkoutInfoPage.typeInFirstNameField("Ionut");
        checkoutInfoPage.typeInLastNameField("Precup");
        checkoutInfoPage.clickOnContinueCheckoutButton();
        assertEquals(checkoutInfoPage.getErrorMessage(), "Address is required", "Expected Error Message to be:Address is required");
    }


}
