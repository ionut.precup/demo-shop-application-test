package org.fasttrackit.Scenarios;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.Account;
import org.fasttrackit.body.Header;
import org.fasttrackit.body.Modal;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.AccountDataProvider;
import org.fasttrackit.pages.MainPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTests extends TestConfiguration {
    MainPage homePage;

    @BeforeTest
    public void setup() {
        homePage = new MainPage();
    }
    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }
    @Test(dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class,
            testName = "Login with valid credentials test", description = "Testing that user can login with valid username and password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_login_on_demo_app_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsNotDisplayed(), "Expected modal to be closed");
        Header header = new Header(account.getUsername());
        assertEquals(header.getGreetingsMessage(), account.getGreetingsMsg(), "Expected greetings message to contain the account user");
        homePage.logUserOut();

    }

    @Test(dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class,
            testName = "Login without password test", description = "Testing that user can't login without password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_without_password_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Please fill in the password!", "Expected error message to be: Please fill in the password!");
        modal.clickOnCloseButton();

    }

    @Test(dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class,
            testName = "Login without username test", description = "Testing that user can't login without username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_without_username_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Please fill in the username!", "Expected error message to be: Please fill in the username!");
        modal.clickOnCloseButton();

    }

    @Test(dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class,
            testName = "Login with invalid username test", description = "Testing that user can't login with wrong username")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_with_wrong_username_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername() + "a");
        modal.typeInPasswordField(account.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "Expected error message to be: Incorrect username or password!");
        modal.clickOnCloseButton();


    }

    @Test(dataProvider = "AccountDataProvider", dataProviderClass = AccountDataProvider.class,
            testName = "Login with invalid password test", description = "Testing that user can't login with wrong password")
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_on_page_with_wrong_password_test(Account account) {
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(account.getUsername() );
        modal.typeInPasswordField(account.getPassword() + "q");
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "Incorrect username or password!", "Expected error message to be: Incorrect username or password!");
        modal.clickOnCloseButton();

    }

    @Test(testName = "Login with locked account test",description = "Testing that user can't login with locked account" )
    @Severity(SeverityLevel.CRITICAL)
    public void user_cant_login_with_locked_account() {
        Account lockedAccount = new Account("locked", "choochoo");
        homePage.clickOnTheLoginButton();
        Modal modal = new Modal();
        modal.typeInUsernameField(lockedAccount.getUsername());
        modal.typeInPasswordField(lockedAccount.getPassword());
        modal.clickOnTheLoginButton();
        assertTrue(homePage.validateModalIsDisplayed(), "Expected modal to remain on page");
        assertEquals(modal.getErrorMessage(), "The user has been locked out.", "Expected error message to be: The user has been locked out.");
        modal.clickOnCloseButton();

    }
}
