package org.fasttrackit.Scenarios;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.dataprovider.ProductDataProvider;
import org.fasttrackit.pages.MainPage;
import org.fasttrackit.pages.ProductsInCartPage;
import org.fasttrackit.products.Product;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ShoppingCartSingleProductTests extends TestConfiguration {
    MainPage homePage = new MainPage();

    @AfterMethod
    public void clean() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }
    @Test(testName = "Product is successfully added to cart test",description = "Testing that Product is successfully added to cart",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void verify_that_the_product_is_successfully_added_to_cart_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        String expectedProductCartTitle = product.getExpectedResults().getTitle();
        assertEquals(cartPage.getProductCartTitle(), expectedProductCartTitle, "Expected product added to cart to be: " + expectedProductCartTitle);

    }
    @Test(testName = "User can remove product from cart test",description = "Testing that user is able to remove product from cart",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_remove_product_from_cart_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        cartPage.removeProductFromCart();
        assertTrue(cartPage.validateCartPageIsEmpty(),"Expected empty Shopping Cart");

    }
    @Test(testName = "User can increment Quantity of product in cart test", description = "Testing that user is able to increment Product quantity in cart",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_increment_quantity_of_product_in_cart_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        cartPage.incrementQuantity();
        cartPage.incrementQuantity();
        assertEquals(cartPage.getQuantity(), "3");

    }

    @Test(testName = "User can decrement Quantity of product in cart test",description = "Testing that user is able to decrement Product quantity in cart",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void user_can_decrement_quantity_of_product_in_cart_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        cartPage.incrementQuantity();
        cartPage.incrementQuantity();
        cartPage.incrementQuantity();
        cartPage.decrementQuantity();
        cartPage.decrementQuantity();
        assertEquals(cartPage.getQuantity(), "2");
    }
    @Test(testName = "Update Product Total Price test", description = "Testing that Product Total Price is updated when quantity is incremented",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void verify_product_total_price_is_correctly_updated_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        cartPage.incrementQuantity();
        cartPage.incrementQuantity();
        cartPage.incrementQuantity();
        assertEquals(cartPage.actualProductTotalPrice(), cartPage.expectedProductTotalPrice(), "Expected Product Total Price to be correctly updated");
    }
    @Test(testName = "Cart Page Product Total Price test",description = "Testing that Product Total Price is correctly calculated for each product",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void verify_product_total_price_is_correctly_displayed_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertEquals(cartPage.actualProductTotalPrice(), cartPage.expectedProductTotalPrice(), "Expected Product Total Price to be correctly displayed");
    }
    @Test(testName = "Tax is applied to Total Amount test", description = "Testing that Tax is applied to Total Amount",
            dataProvider = "productsDataProvider", dataProviderClass = ProductDataProvider.class)
    @Severity(SeverityLevel.CRITICAL)
    public void verify_tax_is_correctly_applied_to_total_amount_test(Product product) {
        product.addProductToBasket();
        homePage.clickOnTheShoppingCartIcon();
        ProductsInCartPage cartPage = new ProductsInCartPage(product);
        assertEquals(cartPage.actualTotal(),cartPage.expectedTotal(),"Expected Tax to be correctly applied to Total Amount");
    }

}
