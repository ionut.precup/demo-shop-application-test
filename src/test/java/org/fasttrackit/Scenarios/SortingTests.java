package org.fasttrackit.Scenarios;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.body.SortDropDownMenu;
import org.fasttrackit.config.TestConfiguration;
import org.fasttrackit.pages.MainPage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SortingTests extends TestConfiguration {
    MainPage homePage = new MainPage();
    @AfterMethod
    public void returnToHomePage() {
        homePage.clickOnTheLogoButton();
        homePage.clickOnTheResetIcon();
    }
    @Test(testName = "Sort by Name Z to A test", description = "Testing that user is able to sort products in descending alphabetical order")
    @Severity(SeverityLevel.NORMAL)
    public void verify_sort_by_name_z_to_a_functionality() {
        List<String> beforeSortProductTitleList = new ArrayList<>();
        for (WebElement p: homePage.getProductTitleList()) {
            beforeSortProductTitleList.add(String.valueOf(p.getText()));
        }
        SortDropDownMenu sortDropDownMenu = new SortDropDownMenu();
        homePage.clickOnTheSortMenu();
        sortDropDownMenu.sortZToA();
        List<String> afterSortProductTitleList = new ArrayList<>();
        for (WebElement p: homePage.getProductTitleList()) {
            afterSortProductTitleList.add(String.valueOf(p.getText()));
        }
        Collections.sort(beforeSortProductTitleList);
        Collections.reverse(beforeSortProductTitleList);
        System.out.println("Sort Result:"+ beforeSortProductTitleList);
        assertEquals(beforeSortProductTitleList,afterSortProductTitleList,"Expected Products to be sorted in descending alphabetical order");

    }
    @Test(testName = "Sort by Price Low To High test", description = "Testing that user is able to sort products by Price in ascending order")
    @Severity(SeverityLevel.NORMAL)
    public void verify_sort_by_price_low_to_high_functionality() {
        List<Double> beforeSortProductPriceList = new ArrayList<>();
        for (WebElement p : homePage.getProductPriceList()) {
            beforeSortProductPriceList.add(Double.valueOf(p.getText().replace("$","")));
        }
        SortDropDownMenu sortDropDownMenu = new SortDropDownMenu();
        homePage.clickOnTheSortMenu();
        sortDropDownMenu.sortLowToHigh();
        sleep(2000);
        List<Double> afterSortProductPriceList = new ArrayList<>();
        for (WebElement p : homePage.getProductPriceList()) {
            afterSortProductPriceList.add(Double.valueOf(p.getText().replace("$","")));
        }
        System.out.println(beforeSortProductPriceList);
        System.out.println(afterSortProductPriceList);
        sleep(1000);
        Collections.sort(beforeSortProductPriceList);
        System.out.println("Sort Result:"+beforeSortProductPriceList);
        assertEquals(beforeSortProductPriceList,afterSortProductPriceList,"Expected Products to be sorted in ascending order");
    }
    @Test(testName = "Sort by Price High To Low test", description = "Testing that user is able to sort products by Price in descending order")
    @Severity(SeverityLevel.NORMAL)
    public void verify_sort_by_price_high_to_low_functionality() {
        List<Double> beforeSortProductPriceList = new ArrayList<>();
        for (WebElement p : homePage.getProductPriceList()) {
            beforeSortProductPriceList.add(Double.valueOf(p.getText().replace("$","")));
        }
        SortDropDownMenu sortDropDownMenu = new SortDropDownMenu();
        homePage.clickOnTheSortMenu();
        sortDropDownMenu.sortHighToLow();

        List<Double> afterSortProductPriceList = new ArrayList<>();
        for (WebElement p : homePage.getProductPriceList()) {
            afterSortProductPriceList.add(Double.valueOf(p.getText().replace("$","")));
        }
        Collections.sort(beforeSortProductPriceList);
        Collections.reverse(beforeSortProductPriceList);
        System.out.println("Sort Result:"+beforeSortProductPriceList);
        assertEquals(beforeSortProductPriceList,afterSortProductPriceList,"Expected Products to be sorted in descending order");
    }


}


