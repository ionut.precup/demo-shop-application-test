package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WishlistPage extends MainPage {
    private final SelenideElement pageSubtitle;
    private final ElementsCollection productsInWishlist;

    public WishlistPage()  {
        this.pageSubtitle = $(".subheader-container .text-muted");
        this.productsInWishlist = $$(".col .card");
    }


    public String getPageSubtitle() {
        return pageSubtitle.text();
    }
    public boolean validateProductsInWishlistAreDisplayed () {
        for (SelenideElement product : this.productsInWishlist) {
            if (!product.isDisplayed()) {
                return false;
            }
        }
        return true;
    }
}
