package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailsPage {
    private final SelenideElement title = $(".subheader-container .text-muted");
    private final SelenideElement price =  $("p[style='font-weight: bold; font-size: 24px;']");

    public String getTitle() {
        return title.text();
    }

    public String getPrice() {
        return price.text();
    }

}


