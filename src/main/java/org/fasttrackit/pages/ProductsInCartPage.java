package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;

import static com.codeborne.selenide.Selenide.*;

public class ProductsInCartPage {
    private final SelenideElement productLinkCart;
    private final SelenideElement container;
    private final SelenideElement plusButton;
    private final SelenideElement minusButton;
    private final SelenideElement trashButton;
    private final SelenideElement continueShoppingButton;
    private final SelenideElement checkoutButton;
    private final SelenideElement unitPrice;
    private final SelenideElement totalPrice;
    private final SelenideElement quantity;
    private final SelenideElement itemsTotal;
    private final SelenideElement tax;
    private final SelenideElement Total;
    private final ElementsCollection productsInCart;
    ElementsCollection cartPriceList;




    public ProductsInCartPage(Product product) {
        this.productLinkCart = $(String.format("#item_%s_title_link", product.getProductId()));
        this.container = productLinkCart.parent().parent();
        this.plusButton = $(".fa-plus-circle");
        this.minusButton = $(".fa-minus-circle");
        this.trashButton = $(".fa-trash ");
        this.continueShoppingButton = $(".btn-danger");
        this.checkoutButton = $(".btn-success");
        this.unitPrice = container.$(".col-md-auto:nth-child(2) div");
        this.totalPrice = container.$(".col-md-auto:nth-child(3) div");
        this.quantity = container.$(".col-md-auto:first-child div");
        this.itemsTotal = $("tr:first-child .amount:last-child");
        this.tax = $("tr:nth-child(2) .amount:last-child");
        this.Total = $(".amount-total .amount:last-child");
        this.cartPriceList = $$(".container .col-md-auto:nth-child(3) div");
        this.productsInCart = $$(".col .container");
    }

    /**
     * Getters
     */
    public ElementsCollection getCartPriceList() {
        return this.cartPriceList;
    }

    public String getProductCartTitle() {
        return productLinkCart.text();
    }


    public String getUnitPrice() {
        return unitPrice.text();
    }

    public String getTotalPrice() {
        return totalPrice.text();
    }

    public String getQuantity() {
        return quantity.text();
    }

    public String getItemsTotal() {
        return itemsTotal.text();
    }

    public String getTax() {
        return tax.text();
    }

    public String getTotal() {
        return Total.text();
    }

    /**
     * Validators
     */
    public boolean validateProductsInCartAreDisplayed () {
        for (SelenideElement product : this.productsInCart) {
            if (!product.isDisplayed()) {
                return false;
            }
        }
        return true;
    }
    public boolean validateCartPageIsEmpty () {
        return this.productsInCart.isEmpty();
    }

    public boolean validateProductCartTitleIsDisplayed() {
        return this.productLinkCart.exists() && this.productLinkCart.isDisplayed();
    }

    public boolean validateIncrementQuantityButtonIsDisplayed() {
        return this.plusButton.exists() && this.plusButton.isDisplayed();
    }

    public boolean validateDecrementQuantityButtonIsDisplayed() {
        return this.minusButton.exists() && this.minusButton.isDisplayed();
    }

    public boolean validateTrashButtonIsDisplayed() {
        return this.trashButton.exists() && this.trashButton.isDisplayed();
    }

    public boolean validateContinueShoppingButtonIsDisplayed() {
        return this.continueShoppingButton.exists() && this.continueShoppingButton.isDisplayed();
    }

    public boolean validateCheckoutButtonIsDisplayed() {
        return this.checkoutButton.exists() && this.checkoutButton.isDisplayed();
    }

    /**
     * Actions
     */

    public void removeProductFromCart() {
        this.trashButton.click();
        sleep(150);
    }

    public void goToCheckout() {
        this.checkoutButton.click();
        sleep(150);
    }

    public void incrementQuantity() {
        this.plusButton.click();
    }

    public void decrementQuantity() {
        this.minusButton.click();
    }


    @Override
    public String toString() {
        return this.productLinkCart.text();
    }
    /**
     * Get Values
     */
    public double productUnitPrice() {
        return Double.parseDouble(getUnitPrice().replace("$", ""));
    }
    public double actualProductTotalPrice() {
        return Double.parseDouble(getTotalPrice().replace("$", ""));
    }
    public int quantityValue () {
        return Integer.parseInt(getQuantity());
    }
    public double actualItemsTotal() {
        return Double.parseDouble(getItemsTotal().replace("$", ""));
    }
    public double tax() {
        return Double.parseDouble(getTax().replace("$", ""));
    }
    public double actualTotal() {
        return Double.parseDouble(getTotal().replace("$", ""));
    }



    /**
     * Expected Results
     */
    public double expectedProductTotalPrice () {
        return productUnitPrice()*quantityValue();
    }

    public double expectedTotal () {
        return expectedItemsTotal()+tax();
    }
    /**
     * Sum all prices
     */
    public double expectedItemsTotal () {
        return productUnitPrice()*quantityValue();
    }







}
