package org.fasttrackit.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.products.Product;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage extends MainPage {
    private final ElementsCollection searchResults;

    public SearchPage(Product product) {
        this.searchResults = $$(String.format(".card-body [href='#/product/%s']", product.getProductId()));
    }

    public List<String> actualSearchResults(){
        return this.searchResults.texts();
    }

}

    

