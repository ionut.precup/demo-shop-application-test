package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.fasttrackit.body.Footer;
import org.fasttrackit.body.Header;

import static com.codeborne.selenide.Selenide.*;

public class MainPage extends Page {
    private final String title = Selenide.title();


    private Header header;
    private final Footer footer;
    private final SelenideElement searchButton = $(".btn-light.btn-sm");
    private final SelenideElement searchField = $("#input-search");
    private final SelenideElement sortField = $(".sort-products-select");
    private final SelenideElement modal = $(".modal-dialog");
    ElementsCollection productTitleList = $$(".card-link");
    ElementsCollection productPriceList = $$(".col .card-text:first-child");

    public ElementsCollection getProductPriceList() {
        return productPriceList;
    }

    public ElementsCollection getProductTitleList() {
        return productTitleList;
    }

    public MainPage() {
        System.out.println("Constructing Header");
        this.header = new Header();
        System.out.println("Constructing Footer");
        this.footer = new Footer();
    }


    /**
     * Getters
     */

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return header;
    }

    public Footer getFooter() {
        return footer;
    }

    /**
     * Validators
     */
    public String verifyThatMainPageTitleIsDisplayed() {
        return title;
    }

    public boolean validateThatSearchButtonIsDisplayed() {
        return this.searchButton.exists() && this.searchButton.isDisplayed();
    }

    public boolean validateModalIsDisplayed() {
        return this.modal.exists() && this.modal.isDisplayed();

    }

    public boolean validateModalIsNotDisplayed() {
        boolean exists = modal.exists();
        return !this.modal.exists();
    }
    public boolean validateSortFieldIsDisplayed() {
        return sortField.exists() && sortField.isDisplayed();
    }
    public boolean validateProductsListIsDisplayed() {
        for (SelenideElement product : this.productTitleList) {
            if (!product.isDisplayed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Actions
     */
    public void clickOnTheSearchButton() {
        this.searchButton.click();
        sleep(100);
    }

    public void clickOnTheSearchField() {
        this.searchField.click();
        sleep(100);
    }

    public void typeInSearchField(String productToType) {
        this.searchField.sendKeys(productToType);
    }

    public void clickOnTheLoginButton() {
        this.header.clickOnTheLoginButton();
        sleep(100);

    }

    public void returnToHomePage() {
        this.header.getLogoIconUrl().click();

    }

    public void clickOnTheShoppingCartIcon() {
        this.header.clickOnShoppingCartIcon();
        sleep(150);
    }

    public void clickOnTheWishlistButton() {
        this.header.clickOnTheWishlistButton();

    }

    public void clickOnTheLogoButton() {
        this.header.clickOnTheLogoButton();
        sleep(100);

    }

    public void logUserOut() {
        this.header.clickOnTheLogoutButton();
    }

    public void clickOnTheResetIcon() {
        this.getFooter().clickOnTheResetIcon();

    }
    public void clickOnTheSortMenu (){
        this.sortField.click();
    }


}

