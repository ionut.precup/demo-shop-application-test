package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class CartPage extends MainPage {


    private final SelenideElement cartPageTitle;
    private final SelenideElement cartPageMessage;


    public CartPage() {

        this.cartPageTitle = $(".subheader-container .text-muted");
        this.cartPageMessage = $(".text-center.container");

    }


    public String getCartPageTitle() {
        return cartPageTitle.text();
    }

    public String getCartPageMessage() {
        return cartPageMessage.text();
    }
}
