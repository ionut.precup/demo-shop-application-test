package org.fasttrackit.body;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class SortDropDownMenu {
   
    private final SelenideElement sortField;
    private final SelenideElement sortAToZ;
    private final SelenideElement sortZToA;
    private final SelenideElement sortLowToHigh;
    private final SelenideElement sortHighToLow;

    public SortDropDownMenu() {
        this.sortField = $(".sort-products-select");
        sortAToZ =  $("[value=az]");
        sortZToA = $("[value=za]");
        sortLowToHigh = $("[value=lohi]");
        sortHighToLow = $("[value=hilo]");
    }
    /**
     * Validators
     */
    public boolean validateSortAtoZIsDisplayed(){
        this.sortAToZ.shouldBe(Condition.appear);
        this.sortAToZ.shouldBe(Condition.selected);
        return this.sortAToZ.exists() && this.sortAToZ.isDisplayed();
    }
    public boolean validateSortZtoAIsDisplayed(){
        this.sortZToA.shouldBe(Condition.appear);
        return this.sortZToA.exists() && this.sortZToA.isDisplayed();
    }
    public boolean validateSortByPriceLowToHighIsDisplayed(){
        this.sortLowToHigh.shouldBe(Condition.appear);
        return this.sortLowToHigh.exists() && this.sortLowToHigh.isDisplayed();
    }
    public boolean validateSortByPriceHighToLowIsDisplayed(){
        this.sortHighToLow.shouldBe(Condition.appear);
        return this.sortHighToLow.exists() && this.sortHighToLow.isDisplayed();
    }
    
    
    /**
     * Actions
     */
    public void clickOnTheSortMenu() {
        this.sortField.click();
    }
    public void sortAToZ () {
        this.sortAToZ.click();
        sleep(150);
    }
    public void sortZToA () {
        this.sortZToA.click();
        sleep(150);

    }
    public void sortLowToHigh () {
        this.sortLowToHigh.click();
        sleep(150);
    }
    public void sortHighToLow () {
        this.sortHighToLow.click();
        sleep(150);
    }


}
