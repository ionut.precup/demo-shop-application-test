package org.fasttrackit.body;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class Modal {
    private final SelenideElement modalTitle = $(".modal-title");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".btn.btn-primary");
    private final SelenideElement errorMessage = $(".error");


    /**
     * Clicks
     */
    public void clickOnCloseButton() {
        System.out.println("Clicked on the 'x' button.");
        this.closeButton.click();
        sleep(150);
    }

    public void clickOnUsernameField() {
        this.username.click();
    }

    public void typeInUsernameField(String userToType) {
        System.out.println("Typed in username: " + userToType);
        this.username.click();
        this.username.sendKeys(userToType);
    }

    public void clickOnPasswordField() {
        this.password.click();

    }

    public void typeInPasswordField(String passwordToType) {
        System.out.println("Typed in password: " + passwordToType);
        this.password.click();
        this.password.sendKeys(passwordToType);
    }

    public void clickOnTheLoginButton() {
        this.loginButton.click();
        sleep(150);
    }

    /**
     * Validators
     */


    public void validateModalComponents() {

    }

    public boolean validateCloseButtonIsDisplayed() {
        return this.closeButton.exists() && this.closeButton.isDisplayed();
    }

    public boolean validateUsernameFieldIsDisplayed() {
        return this.username.exists() && this.username.isDisplayed();
    }

    public boolean validatePasswordFieldIsDisplayed() {
        return this.password.exists() && this.password.isDisplayed();
    }

    public boolean validateLoginButtonIsDisplayedAndIsEnabled() {
        return this.loginButton.isDisplayed() && this.loginButton.isEnabled();
    }


    /**
     * Getters
     */
    public String getErrorMessage() {
        return errorMessage.text();
    }
    public String getModalTitle() {
        return modalTitle.text();
    }



}
