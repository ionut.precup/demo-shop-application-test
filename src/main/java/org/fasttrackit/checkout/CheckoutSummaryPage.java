package org.fasttrackit.checkout;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutSummaryPage {


    public final SelenideElement checkoutSummaryPageTitle;
    public final SelenideElement checkoutItemsTotal;
    public final SelenideElement checkoutTax;
    public final SelenideElement checkoutTotal;
    public final SelenideElement checkoutCancelButton;
    public final SelenideElement completeYourOrderButton;
    public final SelenideElement checkoutPaymentInformation;
    public final SelenideElement checkoutShippingInformation;
    ;


    public CheckoutSummaryPage() {
        this.checkoutSummaryPageTitle = $(".text-muted");
        this.checkoutItemsTotal = $("tr:first-child .amount:last-child");
        this.checkoutTax = $("tr:nth-child(2) .amount:last-child");
        this.checkoutTotal = $(".amount-total .amount:last-child");
        this.checkoutCancelButton = $(".btn.btn-danger");
        this.completeYourOrderButton = $(".btn.btn-success");
        this.checkoutPaymentInformation = $(".col:first-child div:nth-child(2)");
        this.checkoutShippingInformation = $(".col:first-child div:last-child");
    }

    /**
     * Getters
     */
    public String getCheckoutSummaryPageTitle() {
        return checkoutSummaryPageTitle.text();
    }

    public String getCheckoutItemsTotal() {
        return checkoutItemsTotal.text();
    }

    public String getCheckoutTax() {
        return checkoutTax.text();
    }

    public String getCheckoutTotal() {
        return checkoutTotal.text();
    }

    public String getCheckoutPaymentInformation() {
        return checkoutPaymentInformation.text();
    }

    public String getCheckoutShippingInformation() {
        return checkoutShippingInformation.text();
    }

    /**
     * Validators
     */
    public boolean validateCheckoutCancelButtonIsDisplayed() {
        return this.checkoutCancelButton.exists() && this.checkoutCancelButton.isDisplayed();
    }
    public boolean validateCompleteYourOrderButtonIsDisplayed (){
        return this.completeYourOrderButton.exists() && this.completeYourOrderButton.isDisplayed();
    }

    /**
     * Actions
     */
    public void clickOnCompleteYourOrderButton () {
        this.completeYourOrderButton.click();
    }


}
