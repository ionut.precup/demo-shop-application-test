package org.fasttrackit.checkout;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutCompletePage {


    public final SelenideElement checkoutCompletePageTitle;
    public final SelenideElement checkoutCompletePageMessage;
    public final SelenideElement checkoutCompleteContinueShoppingButton;

    public CheckoutCompletePage() {
        this.checkoutCompletePageTitle = $(".text-muted");
        this.checkoutCompletePageMessage = $(".text-center");
        this.checkoutCompleteContinueShoppingButton = $(".btn.btn-success");
    }
    /**
     * Getters
     */
    public String getCheckoutCompletePageTitle() {
        return checkoutCompletePageTitle.text();
    }

    public String getCheckoutCompletePageMessage() {
        return checkoutCompletePageMessage.text();
    }

    /**
     * Validators
     */
    public boolean validateCheckoutCompleteContinueShoppingButtonIsDisplayed() {
        return this.checkoutCompleteContinueShoppingButton.exists() && this.checkoutCompleteContinueShoppingButton.isDisplayed();
    }


    /**
     * Actions
     */
    public void continueShopping (){
        this.checkoutCompleteContinueShoppingButton.click();
    }

}
