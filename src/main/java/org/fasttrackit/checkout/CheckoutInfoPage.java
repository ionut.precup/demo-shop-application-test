package org.fasttrackit.checkout;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class CheckoutInfoPage {


    public final SelenideElement checkoutInfoPageTitle;
    public final SelenideElement addressInformation;
    public final SelenideElement deliveryInformation;
    public final SelenideElement paymentInformation;
    public final SelenideElement firstNameInputField;
    public final SelenideElement lastNameInputField;
    public final SelenideElement addressInputField;
    public final SelenideElement deliveryTypeButton;
    public final SelenideElement cashOnDeliveryButton;
    public final SelenideElement creditCardButton;
    public final SelenideElement payPalButton;
    public final SelenideElement cancelButton;
    public final SelenideElement continueCheckoutButton;

    public final SelenideElement errorMessage;


    public CheckoutInfoPage() {
        this.checkoutInfoPageTitle = $(".text-muted");
        this.addressInformation = $(".col:first-child .section-title:first-child");
        this.deliveryInformation = $(".col:last-child .section-title:first-child");
        this.paymentInformation = $(".section-title:nth-child(3)");
        this.firstNameInputField = $("#first-name");
        this.lastNameInputField = $("#last-name");
        this.addressInputField = $("#address");
        this.deliveryTypeButton = $(".form-check-input[data-test='delivery-type']");
        this.cashOnDeliveryButton = $(".col:last-child div:last-child .form-check:first-child .form-check-input");
        this.creditCardButton = $(".form-check:nth-child(2) .form-check-input");
        this.payPalButton = $(".col:last-child div:last-child .form-check:last-child .form-check-input");
        this.cancelButton = $(".btn.btn-danger");
        this.continueCheckoutButton = $(".btn.btn-success");
        this.errorMessage = $(".error");
    }

    /**
     * Getters
     */

    public String getCheckoutInfoPageTitle() {
        return checkoutInfoPageTitle.text();
    }

    public String getAddressInformation() {
        return addressInformation.text();
    }

    public String getDeliveryInformation() {
        return deliveryInformation.text();
    }

    public String getPaymentInformation() {
        return paymentInformation.text();
    }

    public String getErrorMessage() {
        return errorMessage.text();
    }

    /**
     * Validators
     */

    public boolean validateFirstNameInputFieldIsEnabled() {
        return firstNameInputField.isDisplayed() && firstNameInputField.isEnabled();
    }

    public boolean validateLastNameInputFieldIsEnabled() {
        return lastNameInputField.isDisplayed() && lastNameInputField.isEnabled();
    }


    public boolean validateAddressInputFieldIsEnabled() {
        return addressInputField.isDisplayed() && addressInputField.isEnabled();
    }

    public boolean validateDeliveryTypeOptionIsSelected() {
        return deliveryTypeButton.isSelected();
    }

    public boolean validateCashOnDeliveryOptionIsSelected() {
        return cashOnDeliveryButton.isEnabled();
    }

    public boolean validateCreditCardOptionIsEnabled() {
        return creditCardButton.isEnabled();
    }

    public boolean validatePayPalOptionIsEnabled() {
        return payPalButton.isSelected();
    }

    public boolean validateCancelButtonIsDisplayed() {
        return cancelButton.exists() && cancelButton.isDisplayed();
    }

    public boolean validateContinueCheckoutButtonIsDisplayed() {
        return continueCheckoutButton.exists() && continueCheckoutButton.isDisplayed();
    }


    /**
     * Actions
     */
    public void typeInFirstNameField(String firstNameToType) {
        this.firstNameInputField.sendKeys(firstNameToType);
    }

    public void typeInLastNameField(String lastNameToType) {
        this.lastNameInputField.sendKeys(lastNameToType);
    }

    public void typeInAddressField(String addressToType) {
        this.addressInputField.sendKeys(addressToType);
    }

    public void clickOnContinueCheckoutButton() {
        this.continueCheckoutButton.click();
        sleep(150);
    }

}
