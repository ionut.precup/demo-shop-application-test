# Final project for Demo Shop application testing

A brief presentation of my project

## This is the final project for Ionut Precup, within the FastTrackIt Test Automation course.

### Software engineer: _Rami Sharaiyri_

### Tech stack used:
    - Java17
    - Maven
    - Selenide Framework 
    - TestNG
    - PageObject Models

## Short Description
This application is responsible for testing the main functionalities of the Demo Shop application.
- Static Elements
- Login
- Search
- Sort
- Wishlist
- Shopping Cart
- Checkout
- Product Buy Flow
- End to End 
- Cross Browser testing

### How to run the tests
Git clone https://gitlab.com/ionut.precup/demo-shop-application-test.git
Execute the following commands to:
#### Execute all tests
- `mvn clean tests`
#### Generate Report
- `mvn allure:report`
> Open and present report
- `mvn allure:serve`

#### Page Objects
    - MainPage
    - CartPage
    - ProductDetailsPage
    - ProductsInCartPage
    - SearchPage
    - WishlistPage
    - Header
    - Footer
    - Modal
    - SortDropDownMenu
    - Products
    - CheckoutInfoPage
    - CheckoutSummaryPage
    - CheckoutCompletePage

### Tests implemented
| Test Name                               | Execution Status | Date       |
|-----------------------------------------|------------------|------------|
| Home Page Components Tests              | **PASSED**       | 10.04.2023 |
| Shopping Cart Page Components Tests     | **PASSED**       | 10.04.2023 |
| Modal Components Tests                  | **PASSED**       | 10.04.2023 |
| Sort Menu Components Tests              | **PASSED**       | 10.04.2023 |
| Wishlist Page Components Tests          | **PASSED**       | 10.04.2023 |
| Search Page Components Tests            | **PASSED**       | 10.04.2023 |
| Product Detail Page Components Tests    | **FAILED**       | 10.04.2023 |
| Checkout Info Page Components Tests     | **PASSED**       | 10.04.2023 |
| Checkout Summary Page Components Tests  | **PASSED**       | 10.04.2023 |
| Checkout Complete Page Components Tests | **PASSED**       | 10.04.2023 |
| Login Tests                             | **PASSED**       | 10.04.2023 |
| Search Tests                            | **PASSED**       | 10.04.2023 |
| Shopping Cart Tests                     | **FAILED**       | 10.04.2023 |
| Product Buy Flow Tests                  | **PASSED**       | 10.04.2023 |
| End To End Tests                        | **PASSED**       | 10.04.2023 |
| End To End Tests                        | **PASSED**       | 10.04.2023 |







